const http = require('http')
const express = require('express')
const consola = require('consola')
const SocketIO = require('socket.io')

const { Nuxt, Builder } = require('nuxt')
const app = express()
const server = http.createServer(app)
const io = SocketIO(server)
const registerSockets = require('./sockets')

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'

async function start () {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  await nuxt.ready()
  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  
  // const nsp = io.of('/story-points');
  io.on('connection', socket => {  
    console.log('Connection')
    registerSockets(socket, io)   
  })

  // Listen the server
  server.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}

start()

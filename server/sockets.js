const messages = []

let status = 'ready'

let uid = 0
const makeUid = () => {
  uid += 1 + Math.random()
  return `${uid}`
}

const makeUser = name => {
  const id = makeUid()
  return {
    id,
    name,
    ready: false,
    score: '?'
  }
}

const users = {
}

/*

join - add a user
leave - remove a user by name

change your status => id, status

change 'the' status => 
  
ready => everyone reset
ready => everyone vote secret

reveal => everyone's revealed

*/

const forEachUser = function (fn) {
  for (const id in users) {
    const user = users[id]
    fn(user)
  }
}

module.exports = function (socket, io) {
  const all = () => io.emit
  // socket.on('last-messages', function (fn) {
  //   fn({
  //     messages: messages.slice(-50),
  //     users: users
  //   })
  // })

  // socket.on('send-message', function (message) {
  //   messages.push(message)
  //   socket.broadcast.emit('new-message', message)
  // })

  socket.on('leave', (id, fn) => {
    delete users[id]
    socket.broadcast.emit('users', users)
    fn({
      users
    })
  })

  socket.on('reset', () => {
    users = {}
    io.emit('users', users)
  })

  socket.on('register', fn => {
    console.log('connect')
    fn({
      status,
      users
    })
  })

  socket.on('ready', (id, fn) => {
    status = 'ready'
    forEachUser(user => {
      user.score = '?'
    })
    io.emit('users', users)
    io.emit('status', status)
  })

  socket.on('reveal', (id, fn) => {
    status = 'reveal'
    io.emit('status', status)
  })

  socket.on('change-score', ({ score, id }, fn) => {
    if (users[id]) {
      users[id].score = score
    }
    io.emit('users', users)
  })

  socket.on('logout', id => {
    if (users[id]) {
      delete users[id]
      io.emit('users', users)
    }
  })

  socket.on('join', (name, fn) => {
    const user = makeUser(name)
    users[user.id] = user
    socket.broadcast.emit('users', users)
    fn({
      userId: user.id,
      status,
      users
    })
  })
}
